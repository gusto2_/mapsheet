function startFileManager() {
    YUI({useNativeJSONParse: false}).use(
            'node', 'datasource-io', 'json-parse-shim',
            'datasource-jsonschema', //'datatable-scroll',
            'escape', 'datatable', 'datatable-datasource',
            function(Y) {

                // global UI structure variables
                var folderDatasource;
                var folderTable;
                var fileDatasource;
                var fileTable;


                // building UI bindings
                folderDatasource = new Y.DataSource.IO({
                    source: '/rest/drive/folders',
                    ioConfig: {
                        headers: {
                            'Accept': 'application/json'
                        }
                    }
                });
                folderDatasource.plug(Y.Plugin.DataSourceJSONSchema, {
                    schema: {
                        resultListLocator: 'items',
                        resultFields: ['id', 'title', 'iconLink']
                    }});

                folderTable = new Y.DataTable({
                    columns: [
                        {
                            key: 'iconLink',
                            label: 'icon',
                            formatter: '<img src="{value}"/>',
                            allowHTML: true
                        },
                        'title'],
                    caption: 'Folders',
//                    scrollable: 'y',
//                    height: Y.one("body").get("winHeight"),
                });
                folderTable.plug(
                        Y.Plugin.DataTableDataSource,
                        {datasource: folderDatasource});
                folderTable.render('#folderList');

                folderDatasource.on('response', function(resp) {
                    if (resp.data.status != 200)
                    {
                        message(Y, 'Error: ' + resp.data.responseText);
                    }

                });

                // folder list will be propagated AFTER
                // loading the user info (name, rootId)
                // triggered by the loadUserInfo

                fileDatasource = new Y.DataSource.IO({
                    source: '/rest/drive/files',
                    ioConfig: {
                        headers: {
                            'Accept': 'application/json'
                        }
                    }
                });
                fileDatasource.plug(Y.Plugin.DataSourceJSONSchema, {
                    schema: {
                        resultListLocator: 'items',
                        resultFields: ['id', 'title', 'iconLink', 'mimeType']
                    }});
                fileTable = new Y.DataTable({
                    columns: [
                        {
                            key: 'iconLink',
                            label: 'icon',
                            formatter: '<img src="{value}"/>',
                            allowHTML: true
                        },
                        'title'],
                    caption: 'Files',
                    scrollable: 'y'
                });
                fileTable.plug(
                        Y.Plugin.DataTableDataSource,
                        {datasource: fileDatasource});
                fileTable.render('#fileList');


                // bind UI events
                folderTable.addAttr("selectedRow", {value: null});
                folderTable.delegate('click', function(e) {
                    this.set('selectedRow', e.currentTarget);
                }, '.yui3-datatable-data tr', folderTable);

                folderTable.after('selectedRowChange', function(e) {
                    var tr = e.newVal, // the Node for the TR clicked ...
                            // last_tr = e.prevVal, //  the last TR clicked ...
                            rec = this.getRecord(tr);   // the current Record for the clicked TR 
                            loadFiles(rec.get('id'));
                });

                // bind UI events
                fileTable.addAttr("selectedRow", {value: null});
                fileTable.delegate('click', function(e) {
                    this.set('selectedRow', e.currentTarget);
                }, '.yui3-datatable-data tr', fileTable);

                fileTable.after('selectedRowChange', function(e) {
                    var tr = e.newVal, // the Node for the TR clicked ...
                            // last_tr = e.prevVal, //  the last TR clicked ...
                            rec = this.getRecord(tr);   // the current Record for the clicked TR 
                            var mimeType = rec.get('mimeType');
                            
                            if(mimeType=='application/vnd.google-apps.spreadsheet') {
                                loadFileData(rec.get('id'));
                            }
                            else{
                                message('selected file: '+rec.get('id') + ' is not a google spreadsheet');
                            }
                });
                
                /**
                 * load spreadsheet data
                 * @param {type} fileId
                 * @param {type} fileType
                 * @returns {undefined}
                 */
                function loadFileData(fileId) {
                    message('processing file '+fileId);
                    
                }

                // loading functions
                function message(msg) {
                    var msgNode = Y.one('#message');
                    msgNode.setHTML(Y.Escape.html(msg));
                }

                /* find new authroization request url and redirect the user*/
                function refreshToken() {
                    var tokenDS = new Y.DataSource.IO({
                        source: '/rest/auth/refresh',
                        ioConfig: {
                            headers: {
                                'Accept': 'application/json'
                            }
                        }
                    });
                    tokenDS.sendRequest({
                        on: {
                            success: function(e) {
                                var obj = JSON.parse(e.data.responseText);
                                redirectPage(obj.newAuth);
                            },
                            failure: function(e) {
                                message(e.error.message + ', ' + Y.Escape.html(e.data.responseText));
                            }
                        }
                    });
                }

                /**
                 * loads user info from Google Drive API 
                 * display user name and set root id
                 * @returns {undefined}
                 */
                function loadUserInfo(folderDatasource) {

                    var datasource = new Y.DataSource.IO({
                        source: '/rest/drive/about',
                        ioConfig: {
                            headers: {
                                'Accept': 'application/json'
                            }
                        }
                    });
                    datasource.sendRequest({
                        on: {
                            success: function(e) {
                                var resp = e.data.responseText;
//                message(Y, resp);

                                var obj = Y.JSON.parse(resp);
                                var name = obj.user.displayName;
                                var rootId = obj.rootFolderId;
                                Y.one('#nameField').set('text', name);
                                loadFolders(folderDatasource, rootId, null);
                                loadFiles(rootId, null);
                            },
                            failure: function(e) {
                                //message(Y, e.error.message + ', ' + Y.Escape.html(e.data.responseText));
                                message("Google Drive credentials not valid, trying to refresh, please wait..");
                                if (e.data.status == 403) {
                                    refreshToken();
                                }
                            }
                        }
                    });
                }


                function loadFolders(folderDataSource, rootId, nextPageToken) {
                    if (!nextPageToken)
                    {
                        nextPageToken = '';
                    }
                    folderDataSource.load({
                        'request': '?pageToken=' + nextPageToken,
                    });

                }

                function loadFiles(folderId, nextPageToken) {
                    if (!folderId) {
                        message('Loading files - empty folderId parameter');
                        return;
                    }
                    if (!nextPageToken) {
                        nextPageToken = '';
                    }
                    fileTable.datasource.load({
                        request: '/' + folderId + '?pageToken=' + nextPageToken,
                    });
                }



                // bootstrap function
                loadUserInfo(folderTable.datasource);
            }
    );
}



function redirectPage(url) {
    if (navigator.userAgent.match('/iPhone|iPod/i')) {
        window.location.replace(url);
    } else if (navigator.userAgent.indexOf('Android') != -1) {
        document.location = url;
    } else {
        window.location.href = url;
    }
}
