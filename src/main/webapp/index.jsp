<%-- 
    Document   : index
    Created on : May 16, 2014, 12:43:11 PM
    Author     : admin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="http://yui.yahooapis.com/3.16.0/build/yui/yui-min.js"></script>
        <script type="text/javascript" src="./js/filemanager.js"></script>
        <link rel="stylesheet" href="./css/mapsheet.css" />
        <title>Map sheet</title>
    </head>
    <body class="yui3-skin-mine">
        <div class="logoutDiv">
            <a href="<%=com.apogado.test.mapsheet.User.getLogoutLink()%>">Logout</a>
        </div>
        <div id="message"></div>
        <div class="greeting"><p><span>Hello </span><span id="nameField"></span>
                <input id="rootId" name="rootId" value="" type="hidden"/></p></div>
        <div id="fileSelect">
            <div class="itemList" id="folderList"></div>
            <div class="itemList" id="fileList"></div>
        </div>
        <script>
                startFileManager();
        </script>
    </body>
</html>
