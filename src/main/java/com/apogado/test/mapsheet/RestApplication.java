/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apogado.test.mapsheet;

import com.apogado.test.mapsheet.services.AuthService;
import com.apogado.test.mapsheet.services.DriveServices;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Gabriel
 */
public class RestApplication extends Application {

    private final Set<Object> services = new HashSet<Object>();
    
    public RestApplication() {
        DriveServices driveService = new DriveServices();
        this.services.add(driveService);
        AuthService authService = new AuthService();
        this.services.add(authService);
    }
    
    @Override
    public Set<Object> getSingletons() {
        return this.services;
    }
    
}
