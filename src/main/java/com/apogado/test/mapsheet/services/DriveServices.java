/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apogado.test.mapsheet.services;

import com.google.api.services.drive.Drive;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.apogado.test.mapsheet.oauth.*;
import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.FileList;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import java.io.IOException;
import javax.naming.OperationNotSupportedException;
import javax.servlet.ServletException;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST service around Drive API
 * 
 * /drive/about
 * /drive/folders?pageToken=...
 * 
 * 
 * @author Gabriel
 */
@Path("/drive")
public class DriveServices {

    private static final Logger logger = LoggerFactory.getLogger(DriveServices.class);

    private static final String QUERY_FOLDERS = "mimeType=\'application/vnd.google-apps.folder\'";
    
    // service methods
    @Path("/about")
    @GET
    public Response about(
            @Context HttpServletRequest request) {
        try {
            UserService userService = UserServiceFactory.getUserService();

            AuthorizationCodeFlow authFlow = initializeFlow();
            Credential credential = authFlow.loadCredential(userService.getCurrentUser().getUserId());
            if(credential==null || credential.getExpirationTimeMilliseconds()<System.currentTimeMillis()) {
                logger.warn("Invalid Google Drive credentials");
                return Response.status(403).entity("Credentials not valid").build();
            }
            Drive drive = new Drive.Builder(
                    Utils.HTTP_TRANSPORT, Utils.JSON_FACTORY, credential)
                    .setApplicationName(Utils.APPLICATION_NAME)
                    .build();     
            
            About about = drive.about().get().execute();
            return Response.ok(about.toPrettyString()).build();
        } catch (Exception ex) {
            logger.error("about", ex);
            return Response.serverError().build();
        }
    }

    @Path("/folders")
    @GET
    public Response getFolders(
            @Context HttpServletRequest request,
            @QueryParam("pageToken") String pageToken) {
        try {
            UserService userService = UserServiceFactory.getUserService();
            
            AuthorizationCodeFlow authFlow = initializeFlow();
            Credential credential = authFlow.loadCredential(userService.getCurrentUser().getUserId());
            if(credential==null || credential.getExpirationTimeMilliseconds()<System.currentTimeMillis()) {
                logger.warn("Invalid Google Drive credentials");
                return Response.status(403).entity("Credentials not valid").build();
            }
            Drive drive = new Drive.Builder(
                    Utils.HTTP_TRANSPORT, Utils.JSON_FACTORY, credential)
                    .setApplicationName(Utils.APPLICATION_NAME)
                    .build();    
            
            FileList fileList = drive.files().list().setQ(QUERY_FOLDERS).setPageToken(pageToken).execute();
            return Response.ok(fileList.toPrettyString()).build();
        } catch (Exception ex) {
            logger.error("getFolders", ex);
            return Response.serverError().build();
        }
    }
    
    @Path("/files/{folder}")
    @GET
    public Response getFiles(
            @Context HttpServletRequest request,
            @PathParam("folder") String folderId,
            @QueryParam("pageToken") String pageToken) {
        try {
            if(folderId==null)
            {
                return Response.ok().build();
            }
            UserService userService = UserServiceFactory.getUserService();
            
            AuthorizationCodeFlow authFlow = initializeFlow();
            Credential credential = authFlow.loadCredential(userService.getCurrentUser().getUserId());    
            if(credential==null || credential.getExpirationTimeMilliseconds()<System.currentTimeMillis()) {
                logger.warn("Invalid Google Drive credentials");
                return Response.status(403).entity("Credentials not valid").build();
            }
            Drive drive = new Drive.Builder(
                    Utils.HTTP_TRANSPORT, Utils.JSON_FACTORY, credential)
                    .setApplicationName(Utils.APPLICATION_NAME)
                    .build();                
            
            FileList fileList = drive.files().list().setQ(createFilelistQuesry(folderId)).setPageToken(pageToken).execute();
            return Response.ok(fileList.toPrettyString()).build();
        } catch (Exception ex) {
            logger.error("getFolders", ex);
            return Response.serverError().build();
        }
    }   
    
    @GET @Path("/data/{fileId}")
    public Response readData(@PathParam("fileId") String fileId) {
        
        logger.info("Reading data from file: {}", fileId);
        
        throw new UnsupportedOperationException("Not yet implemented");
    }

    // accessors 
//    private Drive createDriveService(HttpServletRequest req, String userId) throws ServletException, IOException {
//        AuthorizationCodeFlow authFlow = initializeFlow();
//        Credential credential = authFlow.loadCredential(userId);
//        Drive drive = new Drive.Builder(
//                Utils.HTTP_TRANSPORT, Utils.JSON_FACTORY, credential)
//                .setApplicationName(Utils.APPLICATION_NAME)
//                .build();
//        return drive;
//    }

    protected AuthorizationCodeFlow initializeFlow() throws ServletException, IOException {
        return Utils.initializeFlow();
    }
    
    private String createFilelistQuesry(String folderId) {
        StringBuilder buff = new StringBuilder();
        buff.append("mimeType!=\'application/vnd.google-apps.folder\' and '");
        buff.append(folderId).append("\' in parents");
        return buff.toString();
    }

}
