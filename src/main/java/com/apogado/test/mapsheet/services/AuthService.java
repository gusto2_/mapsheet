/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apogado.test.mapsheet.services;

import com.apogado.test.mapsheet.oauth.Utils;
import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.Credential;
import com.google.appengine.api.users.UserServiceFactory;
import java.net.URLEncoder;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Gabriel
 */
@Path("/auth")
public class AuthService {

    private static final Logger logger = LoggerFactory.getLogger(AuthService.class);

    @GET
    @Path("/refresh")
    public Response refresh(@Context HttpServletRequest req) {
        try {
            AuthorizationCodeFlow authFlow = Utils.initializeFlow();
//            Credential credential = authFlow.loadCredential(
//                    UserServiceFactory.getUserService().getCurrentUser().getUserId());
            StringBuilder buff = new StringBuilder();
            buff.append("http://").append(req.getServerName());
            if(req.getLocalPort()!=80 && req.getLocalPort()!=0)
                buff.append(":").append(req.getLocalPort());
            if(req.getContextPath()!=null && req.getContextPath().length()>0)
            {
                buff.append("/").append(req.getContextPath());
            }
            buff.append("/oauth2callback");
            String redirectUri = buff.toString();
            return Response.ok("{ \"newAuth\": \""+authFlow.newAuthorizationUrl().setRedirectUri(redirectUri)+"\"}").build();
        } catch (Exception ex) {
            logger.error("refresh",ex);
            return Response.serverError().entity(ex.toString()).build();
        }

    }
}
