/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apogado.test.mapsheet;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

/**
 *
 * @author Gabriel
 */
public class User {

    public static boolean isLoggedIn() {
        try {
            UserService userService = UserServiceFactory.getUserService();
            return userService.isUserLoggedIn();
        } catch (Exception ex) {
            throw new RuntimeException("Internal exception, cannot proceed");
        }
    }
    
    public static String getLogoutLink() {
         UserService userService = UserServiceFactory.getUserService();
         return userService.createLogoutURL("/");
    }
    
    public static String getUsername() {
        try {
            UserService userService = UserServiceFactory.getUserService();
            if (!userService.isUserLoggedIn()) 
                return null;
            else
                return userService.getCurrentUser().getNickname();
            
        } catch (Exception ex) {
            throw new RuntimeException("Internal exception, cannot proceed");
        }        
    }
    
}
